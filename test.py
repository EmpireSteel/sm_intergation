import requests
import json

def post():
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbix"
    data = {"HPCIntegrInZabbix": {
            "EventID": "210657191",
            "ContactService": 'sv-15992-dir@x5.ru',
            "BriefDescription": "Web-сервер УТМ недоступен",
            "Action": [
                "Проблема: Web-сервер УТМ недоступен",
                "Проблема стартовала:  2019.02.15 в 09:06:02",
                'Завод: BO-O863',
                'Важность проблемы: High',
                'ID проблемы в Zabbix: 210657191'],
        "TemplateName": "1000017"
      }

            }
    headers = {'Content-type': 'application/json'}

    r = requests.post(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    print(r.content.decode("utf-8"))


def put():
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbix/210704132/action/setClosed"
    data = {
            "HPCIntegrInZabbix":
                {
                    "EventID": "210704132"
                }
            }
    headers = {'Content-type': 'application/json'}

    r = requests.put(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    print(r.content.decode("utf-8"))

def get():
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbixQuery/234516932"
    data = {}
    headers = {'Content-type': 'application/json'}

    r = requests.get(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    print(r.content.decode("utf-8"))
    print(r.status_code == 200)

get()


