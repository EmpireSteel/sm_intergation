#!/usr/bin/python
# coding=utf-8
import sys
import requests
import json
import logging
from logging.handlers import RotatingFileHandler
from pyzabbix import ZabbixAPI


def find_template_id(trigger_name):
    if 'Web-сервер УТМ недоступен' in trigger_name:
        return '1000017'
    elif 'CKR FUNCTION FAILED' in trigger_name:
        return '1000018'
    elif 'Аномальное количество токенов JaCarta' in trigger_name:
        return '1000019'
    elif 'проблемы с RSA-ключом' in trigger_name:
        return '1000020'
    elif 'Розничные документы не отправляются более 2 часов' in trigger_name:
        return '1000021'
    else:
        return '0'


def parse_msg(msg):
    zbx_data = {}
    for l in msg.split("\n"):
        l = l.strip()
        if not l:
            continue
        try:
            k, v = l.split(":", 1)
            zbx_data[k] = v
        except:
            logging.error("Can't split line '{}'".format(l))
            continue
    zbx_data['TRIGGER.NAME'] = zbx_data['TRIGGER.NAME'].replace('"', "")
    for k, v in zbx_data.items():
        logger.debug("{} = {}".format(k, v))
    return zbx_data


def create_incident(params, z):
    params['TEMPLATE.ID'] = find_template_id(params['TRIGGER.NAME'])
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbix"
    data = {"HPCIntegrInZabbix": {
        "EventID": params['EVENT.ID'],
        "ContactService": params['HOST.NAME'],
        "BriefDescription": params['TRIGGER.NAME'],
        "Action": [
            "Проблема: " + params['TRIGGER.NAME'],
            "Проблема стартовала: " + params['EVENT.DATE'] + " в " + params['EVENT.TIME'],
            'Завод: ' + params['HOST.NAME'],
            'Важность проблемы: ' + params['TRIGGER.SEVERITY'],
            'ID проблемы в Zabbix: ' + params['EVENT.ID']],
        "TemplateName": params['TEMPLATE.ID']}
    }
    headers = {'Content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    if r.status_code == 200:
        logger.info('HOST: ' + params['HOST.NAME'])
        logger.info('CREATED with event id: ' + params['EVENT.ID'])
        logger.info('CREATED with template id: ' + params['TEMPLATE.ID'])
        z.do_request('event.acknowledge', {'eventids': params['EVENT.ID'],
                                           'message': "SM: created with event id: " + params['EVENT.ID']})
    else:
        logger.error('HOST: ' + params['HOST.NAME'])
        logger.error('ERROR CREATING with event id: ' + params['EVENT.ID'])
        logger.error('ERROR CREATING with template id: ' + params['TEMPLATE.ID'])
        logger.error(r)
        logger.error('Content', r.content)
        logger.error('URL', url)
        logger.error('Data', data)
        z.do_request('event.acknowledge', {'eventids': params['EVENT.ID'],
                                           'message': 'SM: error creating with event id: ' + params['EVENT.ID']})


def close_incident(params, z):
    params['TEMPLATE.ID'] = find_template_id(params['TRIGGER.NAME'])
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbix/{}/action/setClosed".format(params['EVENT.ID'])
    data = {"HPCIntegrInZabbix":{"EventID": params['EVENT.ID']}}
    headers = {'Content-type': 'application/json'}
    r = requests.put(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    if r.status_code == 200:
        logger.info('HOST: ' + params['HOST.NAME'])
        logger.info('CLOSED with event id: ' + params['EVENT.ID'])
        logger.info('CLOSED with template id: ' + params['TEMPLATE.ID'])
        z.do_request('event.acknowledge', {'eventids': params['EVENT.ID'],
                                           'message': 'SM: closed with event id: ' + params['EVENT.ID']})
    else:
        logger.error('HOST: ' + params['HOST.NAME'])
        logger.error('ERROR CLOSING with event id: ' + params['EVENT.ID'])
        logger.error('ERROR CLOSING with template id: ' + params['TEMPLATE.ID'])
        logger.error(r)
        logger.error('Content', r.content)
        logger.error('URL', url)
        logger.error('Data', data)
        z.do_request('event.acknowledge', {'eventids': params['EVENT.ID'],
                                           'message': 'SM: error closing with event id: ' + params['EVENT.ID']})


if __name__ == '__main__':
    z = ZabbixAPI('http://msk-dpro-app351')
    z.login("store_card_api", "12345")
    log_level = logging.INFO
    LOG_PATH = "/var/log/sm_alerts/sm_zabbix.log"
    logFormatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s')
    logger = logging.getLogger()
    logger.setLevel(log_level)
    logfile = LOG_PATH
    fileHandler = logging.FileHandler(logfile)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(log_level)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rotateHandler = RotatingFileHandler(logfile, maxBytes=1024 * 1024 * 5, backupCount=5)
    rotateHandler.setFormatter(logFormatter)
    logger.addHandler(rotateHandler)
    logger.addHandler(consoleHandler)
    logger.info("***********************SM script started***********************")
    logger.debug("Total params: {}".format(len(sys.argv[1:])))
    try:
        title = sys.argv[1]
        msg = sys.argv[2]
        logger.info(title)
        logger.debug(msg)
        zbx_data = parse_msg(msg)
        if 'решена' in title:
            close_incident(zbx_data, z)
        else:
            create_incident(zbx_data, z)
    except Exception as e:
        logger.error(e)
    logger.info("***********************SM script finished***********************\n\n\n")
