import sys
import requests
import json


def close_incident(id):
    url = "http://MSK-DPRO-APP615:15000/SM/9/rest/HPCIntegrInZabbix/1012/action/setClosed"
    data = {
            "HPCIntegrInZabbix":
                {
                    "EventID": "1012"
                }
            }
    headers = {'Content-type': 'application/json'}

    r = requests.put(url, data=json.dumps(data), headers=headers, auth=('integration.zabbix', 'Integr@tion2019'))
    print(r.content.decode("utf-8"))


if __name__ == '__main__':
    close_incident(sys.argv[1])